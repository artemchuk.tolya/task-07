package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBManager {

    private static final DBManager instance = new DBManager();

    private static final String FULL_URL = "jdbc:derby:memory:testdb;create=true";

//    static {
//        Properties prop = new Properties();
//        try (InputStream input = new FileInputStream("app.properties")) {
//            prop.load(input);
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        FULL_URL = prop.getProperty("local.database");
//    }

    public static DBManager getInstance() {
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(DBConstants.GET_ALL_USERS)) {
            users = new ArrayList<>();
            while (rs.next()) {
                users.add(mapToUser(rs));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
        return users;
    }

    private User mapToUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt(DBConstants.F_USERS_ID));
        user.setLogin(rs.getString(DBConstants.F_USERS_LOGIN));
        return user;
    }

    public boolean insertUser(User user) throws DBException {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
            pstmt = con.prepareStatement(DBConstants.ADD_NEW_USER, Statement.RETURN_GENERATED_KEYS);

            int k = 0;
            pstmt.setString(++k, user.getLogin());
            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        } finally {
            close(pstmt);
            close(con);
            close(rs);
        }
        return true;
    }

    private void close(AutoCloseable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection con = DriverManager.getConnection(FULL_URL)) {
            for (User user : users) {
                user = getUser(user.getLogin());
                deleteUser(con, user.getId());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
        return true;
    }

    private void deleteUser(Connection con, int user_id) {
        try (PreparedStatement pstmt = con.prepareStatement(DBConstants.DELETE_USER)) {
            pstmt.setInt(1, user_id);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public User getUser(String login) throws DBException {
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement pstmt = con.prepareStatement(DBConstants.GET_USER_BY_LOGIN)) {
            pstmt.setString(1, login);
            try (ResultSet rs = pstmt.executeQuery()) {
                rs.next();
                return mapToUser(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
    }

    public Team getTeam(String name) throws DBException {
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement pstmt = con.prepareStatement(DBConstants.GET_TEAM_BY_NAME)) {
            pstmt.setString(1, name);
            try (ResultSet rs = pstmt.executeQuery()) {
                rs.next();
                return mapToTeam(rs);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(DBConstants.GET_ALL_TEAMS)) {
            teams = new ArrayList<>();
            while (rs.next()) {
                teams.add(mapToTeam(rs));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
        return teams;
    }

    private Team mapToTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt(DBConstants.F_TEAMS_ID));
        team.setName(rs.getString(DBConstants.F_TEAMS_NAME));
        return team;
    }

    public boolean insertTeam(Team team) throws DBException {
        ResultSet rs = null;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement pstmt = con.prepareStatement(DBConstants.ADD_NEW_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            int k = 0;
            pstmt.setString(++k, team.getName());
            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        } finally {
            close(rs);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        user = getUser(user.getLogin());
        Connection con = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            for (Team team : teams) {
                team = getTeam(team.getName());
                addTeam(con, user.getId(), team.getId());
            }
            con.commit();
        } catch (SQLException | DBException ex) {
            ex.printStackTrace();
            rollback(con);
            throw new DBException("SqlException", ex);
        } finally {
            close(con);
        }
        return true;
    }

    private void addTeam(Connection con, int user_id, int team_id) throws DBException {
        try (PreparedStatement pstmt = con.prepareStatement(DBConstants.ADD_TEAM_FOR_USER)) {
            int k = 0;
            pstmt.setInt(++k, user_id);
            pstmt.setInt(++k, team_id);
            pstmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("Cannot add Team",ex);
        }
    }

    private void rollback(Connection con) {
        try {
            if (con != null) {
                con.rollback();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams;
        user = getUser(user.getLogin());
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement pstmt = con.prepareStatement(DBConstants.GET_ALL_USER_TEAMS)) {
            pstmt.setInt(1, user.getId());
            try (ResultSet rs = pstmt.executeQuery()) {
                teams = new ArrayList<>();
                while (rs.next()) {
                    teams.add(mapToTeam(rs));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean isDeleted;
        team = getTeam(team.getName());
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement pstmt = con.prepareStatement(DBConstants.DELETE_TEAM)) {
            pstmt.setInt(1, team.getId());
            isDeleted = pstmt.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
        return isDeleted;
    }

    public boolean updateTeam(Team team) throws DBException {
        boolean isUpdated;
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement pstmt = con.prepareStatement(DBConstants.UPDATE_TEAM)) {
            pstmt.setString(1, team.getName());
            pstmt.setInt(2, team.getId());
            isUpdated = pstmt.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new DBException("SqlException", ex);
        }
        return isUpdated;
    }

}
