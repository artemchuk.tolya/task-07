package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
    private DBConstants() {
    }

    public static final String F_USERS_ID = "id";
    public static final String F_USERS_LOGIN = "login";
    public static final String F_TEAMS_ID = "id";
    public static final String F_TEAMS_NAME = "name";
    public static final String F_USERS_TEAMS_USER_ID = "user_id";
    public static final String F_USERS_TEAMS_TEAM_ID = "team_id";

    public static final String GET_ALL_USERS = "SELECT * FROM users";
    public static final String ADD_NEW_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    public static final String GET_ALL_TEAMS = "SELECT * FROM teams";
    public static final String ADD_NEW_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    public static final String ADD_TEAM_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";
    public static final String GET_ALL_USER_TEAMS = "SELECT t.id, t.name FROM teams t INNER JOIN users_teams ON t.id = team_id WHERE ? = user_id";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
    public static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
    public static final String DELETE_USER = "DELETE FROM users WHERE id = ?";
    public static final String GET_USER_BY_LOGIN = "SELECT * FROM users WHERE login LIKE ?";
    public static final String GET_TEAM_BY_NAME = "SELECT * FROM teams WHERE name LIKE ?";

}
